import unittest
import spaceinvader


class GameTest(unittest.TestCase):
    def test_game(self):
        """Test : si la variable score de Game vaut bien 0."""
        game = spaceinvader.Game()
        self.assertEqual(game.score, 0)
